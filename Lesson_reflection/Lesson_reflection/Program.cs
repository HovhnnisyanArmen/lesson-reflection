﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Lesson_reflection
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Student> list = CrateStudents(20);
            PrintStudents(list);
            SaveStudentsToXML(list);
            Console.Read();

        }

        static List<Student> CrateStudents(int count)
        {
            var list = new List<Student>(count);
            var rand = new Random();
            for (int i = 0; i < count; i++)
            {
                list.Add(new Student
                {
                    Name = $"A{i + 1}",
                    SurName = $"A{i + 1}yan",
                    age = (byte)rand.Next(18, 50),
                });
            }
            return list;
        }

        static void PrintStudents(List<Student> list)
        {
            foreach (var item in list)
            {
                Type type = item.GetType();
                foreach (var prop in type.GetProperties())
                {
                    Console.WriteLine(prop.Name + "=" + prop.GetValue(item));
                }
                foreach (var prop in type.GetFields())
                {
                    Console.WriteLine(prop.Name + "=" + prop.GetValue(item));
                }
                Console.WriteLine();
            }
        }

        static void SaveStudentsToXML(List<Student> list)
        {
            XDocument xdoc = new XDocument();
            XElement xmlStudents = new XElement("Students");
            foreach (var item in list)
            {
                Type type = item.GetType();
                XElement xmlStudent = new XElement("student");
                foreach (var prop in type.GetProperties())
                {
                    xmlStudent.Add(new XElement(prop.Name, prop.GetValue(item)));
                }
                foreach (var prop in type.GetFields())
                {
                    xmlStudent.Add(new XElement(prop.Name, prop.GetValue(item)));
                }
                xmlStudents.Add(xmlStudent);
            }
            xdoc.Add(xmlStudents);
            xdoc.Save("students.xml");
        }
    }
}
